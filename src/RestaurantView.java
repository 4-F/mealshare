import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;


public class RestaurantView extends JPanel implements Observer{
	private DeliveryModel model;
	private JButton haveFood;
	private JLabel activity;
	private JTable activities;
	
	RestaurantView(DeliveryModel model_) {
		setLayout(new FlowLayout(FlowLayout.CENTER));
		this.model = model_;
		setBackground(Color.pink);
		haveFood = new JButton("I Have Food");
		this.add(haveFood);
		haveFood.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				model.addDelivery();
			}
		});
		activity = new JLabel("Active Deliveries");
		this.add(activity);
		
		
		this.activities = new JTable(5,4);
		this.add(activities);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		this.activities.removeAll();
		int numOfDeliveries = this.model.getNumberOfDeliveries();
		for(int i=0; i< numOfDeliveries; ++i) {
			
			Delivery delivery = model.getDeliveries(i);
			Date d = delivery.getDate();
			this.activities.setValueAt(d.toString(), i, 0);
			this.activities.setValueAt(d.getHours()+":"+ d.getMinutes()+":"+ d.getSeconds(), i, 1);
			this.activities.setValueAt(delivery.getStatus(), i, 3);
			System.out.println(delivery.getStatus());
			System.out.println(delivery.getDate().toString());
		}
	}

}
