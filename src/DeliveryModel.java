import java.util.ArrayList;
import java.util.Observable;


public class DeliveryModel extends Observable{
	private ArrayList<Delivery> deliveries;
	
	DeliveryModel() {
		deliveries = new ArrayList<Delivery>();
	}
	
	public Delivery getDeliveries(int i) {
		return deliveries.get(i);
		
	}
	
	public void addDelivery() {
		this.deliveries.add(new Delivery());
		setChanged();
		notifyObservers();
	}
	
	public void removeDelivery(int i) {
		this.deliveries.remove(i);
		setChanged();
		notifyObservers();
	}
	
	public void setStatus(String status, int index) {
		this.deliveries.get(index).setStatus(status);
		setChanged();
		notifyObservers();
	}
	
	public int getNumberOfDeliveries() {
		return deliveries.size();
	}
}
