import java.awt.Color;
import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;


public class CourierView extends JPanel implements Observer{
	private DeliveryModel model;
	private JButton dropOff;
	private JTable activities;
	
	CourierView(DeliveryModel model_) {
		setBackground(Color.blue);
		setLayout(new FlowLayout(FlowLayout.CENTER));
		this.model = model_;
		dropOff = new JButton("Dropped Off");
		this.add(dropOff);
		
		this.activities = new JTable(5,2);
		this.add(activities);
		
		this.activities.getModel().addTableModelListener(new TableModelListener() {
	    	public void tableChanged(TableModelEvent e) {
	    		if(e.getType() == e.UPDATE) {
	    			int row = e.getLastRow();
	            	int column = e.getColumn();
	            	String oldStatus = model.getDeliveries(row).getStatus();
	            	if( column != 0 && !activities.getValueAt(row, column).equals(oldStatus) ) {
	            	String input = (String) (activities.getValueAt(row, column));
	            	try {
						model.setStatus(input, row);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
	            	System.out.println(input);
	            	}
	    		}
	    	}
	    });
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		this.activities.removeAll();
		int numOfDeliveries = this.model.getNumberOfDeliveries();
		for(int i=0; i< numOfDeliveries; ++i) {
			
			Delivery delivery = model.getDeliveries(i);
			this.activities.setValueAt(delivery.getSupplier(), i, 0);
			this.activities.setValueAt(delivery.getStatus(), i, 1);
			System.out.println(delivery.getStatus());
			System.out.println(delivery.getDate().toString());
		}
	}
}
