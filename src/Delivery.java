import java.util.Date;


public class Delivery {
	private int restaurantID=5;
	private int courierID=9;
	private String status;
	private Date date;
	
	Delivery() {
		this.status = "Open";
		this.date = new Date();
	}
	
	public Date getDate() {
		return this.date;
	}
	
	public void setStatus(String status) {
		this.status=status;
		this.date = new Date();
	}
	
	public int getCourier(){
		return this.courierID;
	}
	public int getSupplier(){
		return this.restaurantID;
	}
	
	public String getStatus() {
		return this.status;
	}
}
