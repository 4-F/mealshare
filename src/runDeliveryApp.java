import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;



public class runDeliveryApp {
	
	public static void main(String[] args) {
		// create Model and initialize it
		playGame( null);

	}
	
	public static void playGame(String word) {
		JFrame frame = new JFrame("Get Food");
		DeliveryModel model = new DeliveryModel();
		// create View, tell it about model (and controller)
		
		RestaurantView resaurantView = new RestaurantView(model);
		CourierView courierView = new CourierView(model);
		
		model.addObserver(resaurantView);
		model.addObserver(courierView);
		// let all the views know that they're connected to the model
		model.notifyObservers();
		
		// create the window
		JPanel p = new JPanel(new GridLayout(1,2));
		frame.getContentPane().add(p);
		p.add(resaurantView);
		p.add(courierView);
		
		frame.setPreferredSize(new Dimension(1000,700));
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
