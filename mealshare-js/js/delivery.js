var app = angular.module('mealShare', []);
app.controller('mealController', function($scope) {
    $scope.deliveries = deliveries;
    $scope.restName = restName;
    $scope.restAddress = restAddress;
    $scope.currentInventory = inventory;
});

function change() {
    var appElement = document.querySelector('[ng-app=mealShare]');
    var $scope = angular.element(appElement).scope();
    $scope.$apply();
}

var deliveries = [];
var numberOfDeliveries = 0;
var inventory = [];

function zeroChecker(x){
    if(x>9){
        return x;
    }
    else {
        return "0"+x;
    }
}

function dateFormat(){
    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    curr_month++;
    var curr_year = d.getFullYear();
    var curr_hours = zeroChecker(d.getHours());
    var curr_minutes =  zeroChecker(d.getMinutes());

    return curr_month + "/" + curr_date + "/" + curr_year + " " + curr_hours + ":" + curr_minutes;
}


function createDelivery(restID, restName, coords) {
	var deliveryID = numberOfDeliveries;

	var infowindow = new google.maps.InfoWindow({
        			content: 	'<div id=popup>' +
                                '<div id=popup-title>'+restName+'</div>' +
                                '<div id=popup-address>'+restAddress+'</div>' +
                                '<br>' +
        						'<div><button style="margin-right:30px" type="button" class="btn btn-success" onclick="acceptMeal('+deliveryID+')"">Accept</button>' +
                                '<button type="button" class="btn btn-danger" onclick="declineMeal('+deliveryID+')"">Decline</button></div>' +
                                '</div>'
    });
	var marker = new google.maps.Marker({
    	position: coords,
    	map: map
    });
	marker.addListener('click', function() {
        infowindow.open(map, marker);
    });

    var delivery = {id:deliveryID, restID:restID, marker:marker, coords:coords, status:"New", timestamp:dateFormat()};
    deliveries.push(delivery);
    numberOfDeliveries++;

    change();
}

function declineDelivery(id){
	for (var i =0; i < deliveries.length; i++)
        if (deliveries[i].id == id) {
            deliveries[i].marker.setMap(null);
            deliveries[i].status = "Declined";
            deliveries[i].timestamp = dateFormat();
            change();
            break;
        }
}

function acceptDelivery(id){
	var infowindow = new google.maps.InfoWindow({
        			content: 	'<div id=popup>' +
                                '<div id=popup-title>'+restName+'</div>' +
                                '<div id=popup-address>'+restAddress+'</div>' +
                                '<br>' +
                                '<div><button type="button" class="btn btn-success" onclick="pickupMeal('+id+')"">Picked Up</button></div>' +
                                '</div>'
    });

    for (var i =0; i < deliveries.length; i++)
        if (deliveries[i].id == id) {
            var marker = new google.maps.Marker({
                position: deliveries[i].coords,
                icon: "resources/accepted.png",
                map: map
            });
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
            deliveries[i].marker.setMap(null);
            deliveries[i].marker = marker;
            deliveries[i].status = "Accepted";
            deliveries[i].timestamp = dateFormat();
            change();
            break;
        }
}

function pickupDelivery(id){
	//timestamp
	for (var i =0; i < deliveries.length; i++)
        if (deliveries[i].id == id) {
            deliveries[i].marker.setMap(null);
            deliveries[i].status = "Picked Up";
            deliveries[i].timestamp = dateFormat();
            inventory.push(0);
            change();
            break;
        }
}

function removeMarker(id){
    for (var i = 0; i < deliveries.length; i++)
        if (deliveries[i].id == id) {
            deliveries[i].marker.setMap(null);
            change();
            break;
        }
    
}

function deliverDelivery(){
	for (var i =0; i < deliveries.length; i++)
        if (deliveries[i].status == "Picked Up") {
            deliveries[i].status = "Delivered";
            deliveries[i].timestamp = dateFormat();

            var dropCoords = {lat: Math.random()*0.01 + 43.647, lng: Math.random()*0.01 - 79.383};

            var infowindow = new google.maps.InfoWindow({
                            content: '<div id=popup>' +
                                    '<div id=popup-title>'+deliveries[i].timestamp+'</div>' +
                                    '<br>' +
                                    '<div><button type="button" class="btn btn-info" onclick="removeMark('+deliveries[i].id+')"">Remove Marker</button></div>' +
                                    '</div>'
            });

            var marker = new google.maps.Marker({
                position: dropCoords,
                icon: "resources/drop.png",
                map: map
            });

            marker.addListener('click', function() {
                infowindow.open(map, marker);
           	});

            deliveries[i].marker=marker;

            inventory.pop();

            change();
            break;
        }
}
