var map;

function initMap() {
	var coords = {lat: 43.647234, lng: -79.383173};
    map = new google.maps.Map(document.getElementById('map'), {
    	zoom: 14,
    	center: coords,
    	zoomControl: true,
    	mapTypeControl: false,
    	scaleControl: false,
    	streetViewControl: false,  
    	rotateControl: false,
    	fullscreenControl: false,
    	styles: [{	featureType: 'all',
    				elementType: 'labels.icon',
    				stylers: [{visibility: 'off'}]}]
    });
}


function acceptMeal(id) {
    acceptDelivery(id);
}

function declineMeal(id) {
	declineDelivery(id);
}

function pickupMeal(id) {
    pickupDelivery(id);
}

function deliverMeal(id) {
    deliverDelivery(id);
}

function removeMark(id) {
    removeMarker(id);
}